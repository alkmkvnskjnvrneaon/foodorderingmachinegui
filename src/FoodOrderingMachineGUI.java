import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {

    void hyou(String food,int kazu){
        if(kazu==1){
            currentText = currentText+food+"\n";
        }
        else if(kazu>1){
            currentText = currentText+food+"×"+kazu+"\n";
        }
    }
    void order(String food){
        int confirmation=JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+"?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation==0){
            currentText ="";
            hyou("Tempura",a);
            hyou("Ramen",b);
            hyou("Udon",c);
            hyou("Doria",d);
            hyou("Moti",f);
            hyou("Pan",g);
            orderedItemsList.setText(currentText);
            String a0LabelText = Integer.toString(a*100+b*200+c*300+d*400+f*500+g*600);
            a0Label.setText(a0LabelText);
        }

    }
    private JPanel root;
    private JLabel toplavel;
    private JButton tempuraButton;
    private JButton udonButton;
    private JButton ramenButton;
    private JTextPane orderedItemsList;
    private JButton misosiruButton;
    private JButton motiButton;
    private JButton doriaButton;
    private JButton button4;
    private JLabel totalLabel;
    private JLabel yenLabel;
    private JLabel a0Label;
    private JButton tempuraButton1;
    private JTextArea textArea1;
    public FoodOrderingMachineGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+1;
                order("Tempura");
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tenpura.jpg")
        ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=b+1;
                order("Ramen");
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c=c+1;
                order("Udon");
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Udon.jpg")
        ));
        doriaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                d=d+1;
                order("Doria");
            }
        });
        doriaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Doria.jpg")
        ));
        motiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f=f+1;
                order("Moti");
            }
        });
        motiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Moti.jpg")
        ));
        misosiruButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                g=g+1;
                order("Pan");
            }
        });
        misosiruButton.setIcon(new ImageIcon(
                this.getClass().getResource("Pan.jpg")
        ));
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to Checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0){
                    String a0LabelText = Integer.toString(a*100+b*200+c*300+d*400+f*500+g*600);
                    JOptionPane.showMessageDialog(null, "Thank you.The Total price is"+a0LabelText+"yen");
                    a=0;b=0;c=0;d=0;f=0;g=0;currentText="";
                    hyou("Tempura",a);
                    hyou("Ramen",b);
                    hyou("Udon",c);
                    hyou("Doria",d);
                    hyou("Moti",f);
                    hyou("Pan",g);
                    a0LabelText = Integer.toString(a*100+b*200+c*300+d*400+f*500+g*600);
                    orderedItemsList.setText(currentText);
                    a0Label.setText(a0LabelText);

                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    static int a=0,b=0,c=0,d=0,f=0,g=0;
    static String currentText;

}